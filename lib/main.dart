import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';


void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Wavy image mask',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: ShaderMaskView(),
    );
  }
}

class ShaderMaskView extends StatefulWidget {
  @override
  _ShaderMask createState() =>
      _ShaderMask();
}

class _ShaderMask extends State<ShaderMaskView> {




  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }


  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();

  }

  @override
  Widget build(BuildContext context) {
   return  Scaffold(

       appBar: new AppBar(
         title: new Text("Yes!!!"),
       ),
       body: new Container(),
       /// Scaffold doesn't exist in this context here
       /// because the context thats passed into 'build'
       /// refers to the Widget 'above' this one in the tree,
       /// and the Scaffold doesn't exist above this exact build method
       ///
       /// This will throw an error:
       /// 'Scaffold.of() called with a context that does not contain a Scaffold.'
       floatingActionButton: Builder(
         builder: (BuildContext context) {

           return FloatingActionButton(onPressed: () {
             Scaffold.of(context).showSnackBar(
               new SnackBar(
                 backgroundColor: Colors.blue,
                 content: new Text('SnackBar'),
               ),
             );
           });

         },

       ));



  }
}



